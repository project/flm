-- SUMMARY --

Failed login messages overrides form messages that are shown to a user
during failed login attempt. You may configure three messages that will be
shown after first, second to last and last login attempt that are controlled
by the maximum limit.
It also allows to alter maximum login limits for both user and IP.

-- INSTALLATION --

* Put the module in your drupal modules directory and enable it in
  admin/modules.
